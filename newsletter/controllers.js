const Router = require('express').Router();

const {
    subscribe,
    notify,
    unsubscribe
} = require('./services.js');

const {
    authorizeUser
} = require('../authorization');

const {
    ADMIN_ROLE,
    READER_ROLE
} = process.env;

Router.get('/subscribe', authorizeUser(ADMIN_ROLE, READER_ROLE), async (req, res) => {
    const {
        userId,
        email
    } = req.state;

    await subscribe(userId, email);

    res.status(204).end();
});

Router.delete('/unsubscribe', authorizeUser(ADMIN_ROLE, READER_ROLE), async (req, res) => {
    const {
        userId
    } = req.state;

    await unsubscribe(userId);

    res.status(204).end();
});

// rute interne, vor fi folosite doar de microservicii

Router.delete('/unsubscribe/user/:userId', async (req, res) => {
    const {
        userId
    } = req.params;

    await unsubscribe(userId);

    res.status(204).end();
});

Router.post('/notify', async (req, res) => {
    const {
        bookName,
        author
    } = req.body;

    await notify(bookName, author);

    res.status(204).end();

});

module.exports = Router;